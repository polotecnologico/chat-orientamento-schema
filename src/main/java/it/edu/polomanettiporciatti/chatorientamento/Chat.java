package it.edu.polomanettiporciatti.chatorientamento;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author 
 * @version 
 */
public class Chat {

	private Chat(){
	}

    /**
     * @param args 	argomenti per la riga di comando. <br>
	 * 				Server: -s <numero_porta> <br>
	 * 				Client: -c <IP_server> <numero_porta>
     */
    public static void main(String[] args) {
        
        // IL TUO CODICE VA SCRITTO QUI
        // ----------------------------

                

        // ----------------------------
		
		
		Future<?> future_ric;
        Future<?> future_tras;
        
        ExecutorService executorService = Executors.newCachedThreadPool();
        
        BufferedReader tastiera = new BufferedReader(new InputStreamReader(System.in));
        
        try {

            if (args[0].equals("-s")) {
				
				if (args.length != 2){
                    System.out.println("Parametri non validi. Digita 'java -jar chat.jar -h' per visualizzare l'help.");
                    return;
				}
				
                // IL TUO CODICE VA SCRITTO QUI
                // ----------------------------



                // ----------------------------
			
			}
            else if (args[0].equals("-c")) {
				
				if (args.length != 3){
                    System.out.println("Parametri non validi. Digita 'java -jar chat.jar -h' per visualizzare l'help.");
                    return;
                }
                
                // IL TUO CODICE VA SCRITTO QUI
                // ----------------------------

                

                // ----------------------------
			
			} 
            else if (args[0].equals("-h")){
				
				if (args.length != 1){
                    System.out.println("Parametri non validi. Digita 'java -jar chat.jar -h' per visualizzare l'help.");
                    return;
                }
                
                // IL TUO CODICE VA SCRITTO QUI
                // ----------------------------

                

                // ----------------------------
			
			} else {
				
				System.out.println("Parametri non validi. Digita 'java -jar chat.jar -h' per visualizzare l'help.");
                return;
			
			}
			
            // IL TUO CODICE VA SCRITTO QUI
            // ----------------------------

                

            // ----------------------------
		
		} catch (IOException | NullPointerException | NumberFormatException ex) {
			
			Logger.getLogger(Chat.class.getName()).log(Level.SEVERE, null, ex);
            return;
		
		}
        
        
        // Codice per la corretta terminazione del programma
		
		// Finché sia il ricevitore che il trasmettitore sono in funzione, attendi
		while(!future_ric.isDone()&&!future_tras.isDone());
		
		// Quando uno dei due termina, termina anche l'altro
        try {
			
			if (future_ric.isDone()){

                System.out.println("Il tuo interlocutore ha chiuso la chat. Premi INVIO per uscire.");
                tastiera.close();
                future_tras.cancel(true);
			
			} else {
				
				channel.close();
                future_ric.cancel(true);
			
			}
			
        } catch (IOException ex) {
			
			Logger.getLogger(Chat.class.getName()).log(Level.SEVERE, null, ex);
		
		}

		// Termina il servizio di esecuzione dei thread
        executorService.shutdown();
        
        System.out.println("Bye!");
        
    }
    
    private static void help(){
        
        System.out.println("Utilizzo: java -jar chat.jar [opzioni]");
        System.out.println("Opzioni per l'utilizzo come server: -s <numero_porta>");
        System.out.println("Opzioni per l'utilizzo come client: -c <IP_server> <numero_porta>");
        System.out.println("Opzioni per visualizzare l'help: -h");
        
    }
    
}
